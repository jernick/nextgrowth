const resultsContainer = document.getElementById("results");
const loader = document.getElementById("loader");
let page = 1; // Initial page
let isLoading = false;

// Function to fetch and append data to the results container
function fetchData() {
  isLoading = true;
  loader.style.display = "block";

  // You can use any publicly available API, here we'll use JSONPlaceholder as an example
  const apiUrl = `https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=15`;

  fetch(apiUrl)
    .then((response) => response.json())
    .then((data) => {
      data.forEach((item) => {
        const resultItem = document.createElement("div");
        resultItem.textContent = item.title;
        resultsContainer.appendChild(resultItem);
      });

      page++;
      isLoading = false;
      loader.style.display = "none";
    })
    .catch((error) => {
      console.error("Error fetching data: ", error);
      isLoading = false;
      loader.style.display = "none";
    });
}

// Function to check if the user has scrolled to the bottom
function isAtBottom() {
  return window.innerHeight + window.scrollY >= document.body.offsetHeight;
}

// Add a scroll event listener
window.addEventListener("scroll", () => {
  if (isAtBottom() && !isLoading) {
    fetchData();
  }
});

// Initial data load
fetchData();
